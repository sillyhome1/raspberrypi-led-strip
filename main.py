#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# TODO: check connection status and re-register every now and then (might just be a issue with my pi)

import os
import time
import _thread
import logging
import requests
import argparse
import RPi.GPIO as GPIO
from http.server import HTTPServer


from config import Config
from led_manager import LED_manager
from http_handler import ServerHandler


def create_ssl_cert(cert_destination: str, key_destination: str):
    from subprocess import run

    try:
        ssl_exec_list = [
            "openssl",
            "req",
            "-new",
            "-newkey",
            "rsa:2048",
            "-x509",
            "-sha256",
            "-days",
            "365",
            "-nodes",
            "-out",
            cert_destination,
            "-keyout",
            key_destination,
            "-subj",
            "/C=/ST=/L=/O=/CN=",
        ]
        run(ssl_exec_list)
    except FileNotFoundError as e:
        logging.error("openssl executable not found!")
        raise e

    logging.info("Self signed ssl certificate created at {}".format(cert_destination))
    logging.info("SSL key created at {}".format(key_destination))


def start_server(server_class=HTTPServer, handler_class=ServerHandler):
    port = Config.RECEIVING_PORT

    server_address = ("", port)
    httpd = server_class(server_address, handler_class)

    if Config.PROVIDE_HTTPS:
        import ssl

        if not os.path.isfile(Config.SSL_CERT_FILE) or not os.path.isfile(
            Config.SSL_KEY_FILE
        ):
            create_ssl_cert(Config.SSL_CERT_FILE, Config.SSL_KEY_FILE)

        httpd.socket = ssl.wrap_socket(
            httpd.socket,
            certfile=Config.SSL_CERT_FILE,
            keyfile=Config.SSL_KEY_FILE,
            server_side=True,
        )

    logging.info("Starting httpd...")
    try:
        httpd.serve_forever()
    except Exception as e:
        httpd.server_close()
        logging.info("Stopping httpd...")
        raise e


def button_callback(*args):
    logging.debug("button pressed")

    if LED_manager.current_effect != LED_manager.last_non_off_effect:
        logging.debug("playing last effect {}".format(LED_manager.last_non_off_effect))
        _thread.start_new_thread(LED_manager.handle, (LED_manager.last_non_off_effect,))
    else:
        logging.debug("turning LEDs off")
        off = {"infos": {"effect_group": "static-color", "effect_value": "000000"}}
        _thread.start_new_thread(LED_manager.handle, (off,))


def setup_on_off_button():
    logging.debug("configuring on/off button")
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(Config.SLAVE_ON_OFF_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    GPIO.add_event_detect(
        Config.SLAVE_ON_OFF_PIN, GPIO.RISING, callback=button_callback, bouncetime=200
    )

    logging.debug("configured on/off button")


def _registerToMaster() -> requests.Response:
    callback = f":{Config.RECEIVING_PORT}{Config.CALLBACK_URL}"

    data = {
        "callback_url": callback,
        "ssl": Config.USE_SELF_SIGNED_CERT,
    }

    headers = {"Authorization": f"Bearer {Config.AUTH_TOKEN}"}

    timeout = 10

    response = requests.post(
        url=Config.MASTER_URI,
        verify=Config.VALIDATE_CERT,
        stream=True,
        data=data,
        headers=headers,
        timeout=timeout,
    )
    statusCode = response.status_code

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logging.error(
            f"http error {statusCode} encountered when initially contacting the master server {Config.MASTER_URI}"
        )
        raise e
    return response


def start():
    # initialize the config Object
    Config()

    logging.basicConfig(
        format=Config.LOG_FORMAT, filename=Config.LOG_FILE, level=Config.LOG_LEVEL
    )
    logging.info("starting...")
    led_manager = LED_manager()

    if Config.ENABLE_SOURCE_VALIDATION:
        # initially messaging the server
        callback_url = ":{}".format(Config.RECEIVING_PORT)

        if Config.CALLBACK_URL == "":
            callback_url += "/" + str(time.time_ns())
        else:
            callback_url += (
                Config.CALLBACK_URL
                if Config.CALLBACK_URL[0] == "/"
                else ("/" + Config.CALLBACK_URL)
            )

        logging.info("registering with the server")

        r = _registerToMaster()

        if r.status_code == 226:
            logging.info("server responded with the effect: {}".format(r.json()))
            _thread.start_new_thread(LED_manager.handle, (r.json(),))

    if Config.SLAVE_USE_ON_OFF_BUTTON:
        setup_on_off_button()

    # starting the LED slave "service"
    try:
        start_server()
    except BaseException:
        # clean up the certificates
        if Config.PROVIDE_HTTPS:
            os.remove(Config.SSL_CERT_FILE)
            os.remove(Config.SSL_KEY_FILE)
            logging.info(
                "removed certificate and key files {} and {}".format(
                    Config.SSL_CERT_FILE, Config.SSL_KEY_FILE
                )
            )

        # delete slave from master
        # r = requests.delete(url=Config.MASTER_URI, verify=False)
        pass
    quit(0)


def stop():
    try:
        if Config.PROVIDE_HTTPS:
            requests.delete("https://localhost", timeout=0.5, verify=False)

        else:
            requests.delete("http://localhost", timeout=0.5)

    except requests.exceptions.ConnectionError:
        pass

    quit(0)


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    group = parser.add_mutually_exclusive_group()
    group.add_argument("--start", help="start the slave service", action="store_true")

    group.add_argument("--stop", help="stop the slave service", action="store_true")

    args = parser.parse_args()

    if args.start:
        start()
    elif args.stop:
        stop()


if __name__ == "__main__":
    main()
