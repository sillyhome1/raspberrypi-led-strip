import configparser


class Config:
    # "generic" stuff
    ENABLE_SOURCE_VALIDATION = True
    CALLBACK_URL = ""
    MASTER_URI = "https://192.168.0.1/leds"
    MASTER_IP = ""
    PROVIDE_HTTPS = False
    SSL_CERT_FILE = "certificate.crt"
    SSL_KEY_FILE = "key.key"
    SLAVE_USE_ON_OFF_BUTTON = False
    SLAVE_ON_OFF_PIN = 23
    VALIDATE_CERT = False
    RECEIVING_PORT = 80
    AUTH_TOKEN = ""

    # log specific stuff
    LOG_FILE = "IoT_LED_Slave.log"
    LOG_LEVEL = 20
    LOG_FORMAT = "%(asctime)s|%(levelname)s|%(funcName)s|%(message)s"

    # mostly low level led stuff
    LED_COUNT = 18
    LED_PIN = 12
    LED_FREQ_HZ = 800000
    LED_DMA = 10
    LED_BRIGHTNESS = 255
    LED_INVERT = False
    LED_CHANNEL = 0
    LED_FPS = 30
    LED_DEFAULT_EFFECT = (
        '{"infos":{"effect_group":"static-color","effect_value":"baDa55"}}'
    )

    def __init__(self):
        configfile = "slave.ini"

        config = configparser.RawConfigParser()
        config.read(configfile)

        Config.ENABLE_SOURCE_VALIDATION = config.getboolean(
            "DEFAULT",
            "ENABLE_SOURCE_VALIDATION",
            fallback=Config.ENABLE_SOURCE_VALIDATION,
        )

        Config.CALLBACK_URL = config.get(
            "DEFAULT", "CALLBACK_URL", fallback=Config.CALLBACK_URL
        )
        if Config.CALLBACK_URL[:1] != "/":
            Config.CALLBACK_URL = "/" + Config.CALLBACK_URL

        Config.MASTER_URI = config.get(
            "DEFAULT", "MASTER_URI", fallback=Config.MASTER_URI
        )

        Config.PROVIDE_HTTPS = config.getboolean(
            "DEFAULT", "USE_SELF_SIGNED_CERT", fallback=Config.PROVIDE_HTTPS
        )

        Config.SSL_CERT_FILE = config.get(
            "DEFAULT", "SSL_CERT_FILE", fallback=Config.SSL_CERT_FILE
        )

        Config.SSL_KEY_FILE = config.get(
            "DEFAULT", "SSL_KEY_FILE", fallback=Config.SSL_KEY_FILE
        )

        Config.VALIDATE_CERT = config.getboolean(
            "DEFAULT", "VALIDATE_CERT", fallback=Config.VALIDATE_CERT
        )

        Config.RECEIVING_PORT = config.getint(
            "DEFAULT", "RECEIVING_PORT", fallback=Config.RECEIVING_PORT
        )

        Config.AUTH_TOKEN = config.get("DEFAULT", "AUTH_TOKEN", fallback="")

        # GPIO shit
        Config.SLAVE_USE_ON_OFF_BUTTON = config.getboolean(
            "DEFAULT",
            "SLAVE_USE_ON_OFF_BUTTON",
            fallback=Config.SLAVE_USE_ON_OFF_BUTTON,
        )

        Config.SLAVE_ON_OFF_PIN = config.getint(
            "DEFAULT", "SLAVE_ON_OFF_PIN", fallback=Config.SLAVE_ON_OFF_PIN
        )

        # log shit
        Config.LOG_FILE = config.get("LOGGING", "LOG_FILE", fallback=Config.LOG_FILE)

        Config.LOG_LEVEL = config.getint(
            "LOGGING", "LOG_LEVEL", fallback=Config.LOG_LEVEL
        )

        Config.LOG_FORMAT = config.get(
            "LOGGING", "LOG_FORMAT", fallback=Config.LOG_FORMAT
        )

        # led shit
        Config.LED_COUNT = config.getint("LED", "LED_COUNT", fallback=Config.LED_COUNT)

        Config.LED_PIN = config.getint("LED", "LED_PIN", fallback=Config.LED_PIN)

        Config.LED_FREQ_HZ = config.getint(
            "LED", "LED_FREQ_HZ", fallback=Config.LED_FREQ_HZ
        )

        Config.LED_DMA = config.getint("LED", "LED_DMA", fallback=Config.LED_DMA)

        Config.LED_BRIGHTNESS = config.getint(
            "LED", "LED_BRIGHTNESS", fallback=Config.LED_BRIGHTNESS
        )

        Config.LED_INVERT = config.getboolean(
            "LED", "LED_INVERT", fallback=Config.LED_INVERT
        )

        Config.LED_CHANNEL = config.getint(
            "LED", "LED_CHANNEL", fallback=Config.LED_CHANNEL
        )

        Config.LED_FPS = config.getint("LED", "LED_FPS", fallback=Config.LED_FPS)

        Config.LED_DEFAULT_EFFECT = config.get(
            "LED", "LED_DEFAULT_EFFECT", fallback=Config.LED_DEFAULT_EFFECT
        )
