# IoT LED slave

This is a python script for Raspberry Pis with LED strips that enable multiple Pis to be controlled via one master server.  
A implementation for a management interface can be found [here](https://gitlab.com/peeteer1245/iot-led-management) by @Truzze.

[[_TOC_]]

# Usage

1. set the configs in `slave.ini`

   - the most important are `MASTER_URI`, `LED_COUNT`, and `LED_PIN`

1. start the script `python3 slave.py --start`

optional:

- 'install' this with `install.py -i` and start/enable the service with `systemctl --now enable led-slave`
  - this adds it to systemd
- you might want to disable the wifi power management
  - I added `/sbin/iw wlan0 set power_save off` to `/etc/rc.local` right before `exit 0` to have it set at startup

# Electrical

For my LED strips I used a 5m \* 30led/m strip of ws2812b's and a 5V 10A power supply for the strip & Raspberry.  
As slave I used a Raspberry Pi Zero Wireless.

I connected the LED data line to GPIO18 (PWM0)(pin 12) and the Power supply 5V/Ground to both the LED strip and Raspberry Pi. Additionally I attached 5V and GPIO23 (PIN 16) to a momentary switch. You might want to use a shielded cable for the button like a CAT5e and ground it's shielding to mitigate EMI.

![electrical diagramm](img/electrical_diag.png)

# API

The slave first registers itself on its master server.  
There should be a callback-url for registering and deregistering, both with the same URL but accessible via different HTTP verbs, POST and DELETE respectively.  
The slave should send where it wants to be contacted and if it wants to be contacted via HTTPS. These values have the names `slave_endpoint` and `https`.

The Server may immediately respond with the current LED settings in the format described below inside a JSON object. In that case the server gives a status code of 226.  
If the server doesn't immediately give a LED status, he sends a status code of 201.

When a server wants to change the state of a slave it sends a POST request to the slave at the given endpoint with the selected `effect_group` and `effect_value` values. This information is transmitted as JSON in a value called `infos`. Additionally it needs to include a RFC-6750 compliant header with the bearer token.

Example post request to register a slave:
`{"slave_endpoint": "/bedroom", "ssl": "true"}`

Here is a example post request to affect a slave:
`{"infos":{"effect_group":"static-color","effect_value":"bada55"}}`

The server may at any time request the currently running effect under the slave endpoint with a `GET` request. The response is a JSON file with the same format as the server sends the slave.

If the slave wants to exit this relationship, it sends the server a DELETE request. It could also just completely exit/die and not respond.

# Effects

All compatible effect groups are listed here with their necessary `effect_value`s and example post requests.

- `static-color`

  - The key `effect_value` contains one string. This string is the desired colour in HEX format. It is case agnostic, a '#' prefix is optional and can be a three or six letter long colour.

  1. `{"infos":{"effect_group":"static-color","effect_value":"baDa55"}}`

- `rainbow-wave`

  - The key `effect_value` contains one or two more keys.
    - `duration`
      - A float that determines how long in seconds a full transition of the rainbow takes on the led strip.
    - `reverse`
      - Boolean that tells the slave to display the effect in reverse.
      - defaults to false

  1. `{"infos":{"effect_group":"rainbow-wave","effect_value":{"duration":8}}}`
  1. `{"infos":{"effect_group":"rainbow-wave","effect_value":{"duration":5.1,"reverse":True}}}`

- `rainbow-static`

  - The key `effect_value` contains one or two more keys.
    - `duration`
      - A float that determines how long in seconds a full transition of the rainbow takes on the led strip.
    - `reverse`
      - Boolean that tells the slave to display the effect in reverse.
      - defaults to false

  1. `{"infos":{"effect_group":"rainbow-static","effect_value":{"duration":8}}}`
  1. `{"infos":{"effect_group":"rainbow-static","effect_value":{"duration":5.1,"reverse":True}}}`

- `breathing`

  - The key `effect_value` contains two more keys.
    - `colours`
      - A array of colour codes as strings.
    - `duration`
      - A float that determines how long in seconds a transition from one colour to the next takes on the led strip.

  1. `{"infos":{"effect_group":"breathing","effect_value":{"colours":["FF0000","2bad20","0000FF"],"duration":4.0}}}`

- `campfire`

  - `hue`
    - What color hue the Candlelight should have.
  - `turbulence`
    - How extremely the "campfire" flickers. 1 is a normal amount, 2 the candle is almost getting blown out.

  1. `{"infos":{"effect_group":"campfire","effect_value":{"hue":50,"turbulence":1.5}}}`

- `comet`

  - `hue`
    - What color hue the Comet should have.
  - `speed`
    - Relative speed to the base speed. 1 is normal speed, 5 is 5x as fast
  - `reverse`
    - Boolean that tells the slave to display the effect in reverse.
    - defaults to false

  1. `{"infos":{"effect_group":"comet","effect_value":{"hue":15,"speed":1.5}}}`

- `rainbow-sine-wave`

  - The key `effect_value` contains one or two more keys.
    - `brightness`
      - A float between 0 and 1 that determines a brightness adjustment. 0.5 would be normal brightness. Above that and the colours become lighter.
    - `duration`
      - A float that determines how long in seconds a full transition of the rainbow takes on the led strip.
    - `reverse`
      - Boolean that tells the slave to display the effect in reverse.
      - defaults to false

  1. `{"infos":{"effect_group":"rainbow-sine-wave","effect_value":{"brightness": 0.5, "duration":8}}}`
  1. `{"infos":{"effect_group":"rainbow-sine-wave","effect_value":{"brightness": 0.2, "duration":5.1,"reverse":True}}}`

- `rainbow-sine-static`

  - The key `effect_value` contains one or two more keys.
    - `brightness`
      - A float between 0 and 1 that determines a brightness adjustment. 0.5 would be normal brightness. Above that and the colours become lighter.
    - `duration`
      - A float that determines how long in seconds a full transition of the rainbow takes on the led strip.
    - `reverse`
      - Boolean that tells the slave to display the effect in reverse.
      - defaults to false

  1. `{"infos":{"effect_group":"rainbow-sine-static","effect_value":{"brightness": 0.5, "duration":8}}}`
  2. `{"infos":{"effect_group":"rainbow-sine-static","effect_value":{"brightness": 0.7, "duration":5.1,"reverse":True}}}`

- `progress-bar`

  - The key `effect_value` contains three more keys.
    - `colours`
      - A array of colour codes as strings.
    - `duration`
      - A float that determines how long in seconds a transition from one colour to the next takes on the led strip.
    - `count`
      - The amount of simultaneous loading bars as integer from 0 to infinity.
    - `reverse`
      - Boolean that tells the slave to display the effect in reverse.
      - defaults to false

  1. `{"infos":{"effect_group":"progress-bar","effect_value":{"colours":["FF0000","2bad20","0000FF"],"duration":6.9, "count": 4, "reverse":False}}}`

# Dependencies

- [requests](https://pypi.org/project/requests/)
- [rpi_ws281x](https://pypi.org/project/rpi-ws281x/)
- [RPi.GPIO](https://pypi.org/project/RPi.GPIO/)
