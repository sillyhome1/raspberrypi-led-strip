import json
import _thread
import logging
import urllib.parse


from http.server import BaseHTTPRequestHandler


from config import Config
from led_manager import LED_manager


class ServerHandler(BaseHTTPRequestHandler):
    def log_message(self, format, *args):
        # mostly silence the terminal
        return

    def _set_response(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

    def do_DELETE(self):
        logging.debug("received DELETE request")
        if self.client_address[0] == "127.0.0.1" or self.client_address[0] == "::1":
            logging.debug("received delete from valid source")
            self.send_response(200)
            self.wfile.write("OK\n".encode("utf-8"))
            self.finish()
            self.connection.close()
            quit(0)
        logging.debug("received delete from invalid source")
        self.send_response(403)
        self.wfile.write("Unauthorized\n".encode("utf-8"))
        return

    def do_POST(self):
        content_length = int(
            self.headers["Content-Length"]
        )  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself

        logging.debug(
            "POST request for {} with {} from {}".format(
                self.path,
                urllib.parse.unquote_plus(post_data.decode("utf-8")),
                self.client_address[0],
            )
        )

        if not self.is_from_valid_source():
            return

        logging.debug("request accepted")

        POST = json.loads(post_data.decode("utf-8"))

        _thread.start_new_thread(LED_manager.handle, (POST,))

        self._set_response()
        self.wfile.write("ok\n".encode("utf-8"))

    def do_GET(self):
        logging.debug(
            "GET request for {} from {}".format(self.path, self.client_address[0])
        )

        if not self.is_from_valid_source():
            return

        self._set_response()
        self.wfile.write(json.dumps(LED_manager.current_effect).encode("utf-8"))

    def is_from_valid_source(self):
        if not Config.ENABLE_SOURCE_VALIDATION:
            return True

        bearer_header = self.headers.get("Authorization")
        auth_token = bearer_header.split(" ")[1]

        if Config.AUTH_TOKEN != auth_token:
            clientIP = self.client_address[0]
            logging.critical(f"unauthorized request from {clientIP}")

            self._set_response()
            self.send_response(403)
            self.wfile.write("no\n".encode("utf-8"))
            return False
        return True
