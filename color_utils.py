from typing import Tuple


def rgb_transition(
    fraction: float, r1: float, g1: int, b1: int, r2: int, g2: int, b2: int
) -> Tuple[int, int, int]:
    # finds arbitrary point between two colours linearly
    return (
        int((1 - fraction) * r1 + fraction * r2),
        int((1 - fraction) * g1 + fraction * g2),
        int((1 - fraction) * b1 + fraction * b2),
    )


def HSV_to_RGB(h: int, s: float, v: float) -> Tuple[int, int, int]:
    if s == 0.0:
        return int(v * 255), int(v * 255), int(v * 255)

    while h > 360:
        h -= 360
    while h < 0:
        h += 360

    h /= 360
    i = int(h * 6.0)  # XXX assume int() truncates!
    f = (h * 6.0) - i
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - s * (1.0 - f))
    i = i % 6
    if i == 0:
        return int(v * 255), int(t * 255), int(p * 255)
    if i == 1:
        return int(q * 255), int(v * 255), int(p * 255)
    if i == 2:
        return int(p * 255), int(v * 255), int(t * 255)
    if i == 3:
        return int(p * 255), int(q * 255), int(v * 255)
    if i == 4:
        return int(t * 255), int(p * 255), int(v * 255)
    # i must be 5
    return int(v * 255), int(p * 255), int(q * 255)


def HEX_to_RGB(rgb_hex: str) -> Tuple[int, int, int]:
    rgb_hex = rgb_hex.replace("#", "")
    if len(rgb_hex) == 3:
        return (
            int(rgb_hex[0:1] * 2, 16),
            int(rgb_hex[1:2] * 2, 16),
            int(rgb_hex[2:3] * 2, 16),
        )
    elif len(rgb_hex) == 6 or len(rgb_hex) == 8:
        return int(rgb_hex[0:2], 16), int(rgb_hex[2:4], 16), int(rgb_hex[4:6], 16)

    else:
        return 0, 0, 0
