#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import subprocess

service_dst = "/etc/systemd/system/led-slave.service"


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-i", "--install", help="install the slave service", action="store_true"
    )

    group.add_argument(
        "-r", "--remove", help="remove the slave service", action="store_true"
    )

    args = parser.parse_args()

    if args.install:
        install()
    elif args.remove:
        remove()


def raise_for_process_success(process=subprocess.CompletedProcess):
    if process.returncode != 0:
        raise subprocess.CalledProcessError


def install():
    global service_dst

    print("reading template")
    service_template = "".join(open("led-slave.service", "r").readlines())

    print("filling template")
    service_file = service_template.format(os.getcwd())

    print("writing service file")
    open(service_dst, "w").write(service_file)

    print(
        "\neverything seems to have worked\
        \nyou can try starting and/or\
        \nenabling the led-slave service"
    )


def remove():
    global service_dst
    if os.path.isfile(service_dst):
        print("stopping the service")
        try:
            ret = subprocess.run(("systemctl", "stop", "led-slave.service"))
            raise_for_process_success(ret)
        except FileNotFoundError:
            print("service file not found")

        print("disabling the service")
        try:
            ret = subprocess.run(("systemctl", "disable", "led-slave.service"))
            raise_for_process_success(ret)
        except FileNotFoundError:
            print("service file not found")

        print("removing the service")
        try:
            os.remove(service_dst)
        except FileNotFoundError:
            print("service file not found")

        print("the service should be removed now")


if __name__ == "__main__":
    main()
