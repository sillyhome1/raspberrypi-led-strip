import math
import json
import time
import random
import logging


from threading import Lock

from rpi_ws281x import PixelStrip, Color

from color_utils import rgb_transition, HSV_to_RGB, HEX_to_RGB
from config import Config


class LED_manager(object):
    lock = Lock()
    request_lock = Lock()
    strip = None
    supported_groups = [
        "breathing",
        "campfire",
        "comet",
        "progress-bar",
        "rainbow-sine-static",
        "rainbow-sine-wave",
        "rainbow-static",
        "rainbow-wave",
        "static-color",
    ]

    current_effect = {
        "infos": {"effect_group": "static-color", "effect_value": "000000"}
    }

    last_non_off_effect = json.loads(Config.LED_DEFAULT_EFFECT)

    def __init__(self) -> None:
        LED_manager.strip = PixelStrip(
            Config.LED_COUNT,
            Config.LED_PIN,
            Config.LED_FREQ_HZ,
            Config.LED_DMA,
            Config.LED_INVERT,
            Config.LED_BRIGHTNESS,
            Config.LED_CHANNEL,
        )
        LED_manager.strip.begin()

    @staticmethod
    def handle(POST: dict) -> None:
        # check if all needed post variables are included
        if "effect_group" not in POST["infos"] or "effect_value" not in POST["infos"]:
            logging.warning("invalid POST values: {}".format(POST.__str__()))
            return

        # check if the wanted effect is supported
        if POST["infos"]["effect_group"] not in LED_manager.supported_groups:
            logging.warning(
                'unsupported effect: "{}"'.format(POST["infos"]["effect_group"])
            )
            return

        # set last and current effects
        LED_manager.current_effect = POST

        if (
            POST["infos"]["effect_value"] != "000000"
            and POST["infos"]["effect_value"] != "#000000"
            and POST["infos"]["effect_value"] != "000"
            and POST["infos"]["effect_value"] != "#000"
        ):
            LED_manager.last_non_off_effect = POST

        logging.debug("requesting lock")

        # ask for the lock
        LED_manager.request_lock.acquire()
        LED_manager.lock.acquire()
        LED_manager.request_lock.release()

        # launch the wanted effect
        logging.debug("acquired lock")
        logging.info(
            "executing effect {}  with parameter {}".format(
                POST["infos"]["effect_group"], POST["infos"]["effect_value"]
            )
        )

        def __validate_reverse(_POST) -> bool:
            return (
                "reverse" in _POST["infos"]["effect_value"]
                and _POST["infos"]["effect_value"]["reverse"] != False
                and (
                    _POST["infos"]["effect_value"]["reverse"] == True
                    or _POST["infos"]["effect_value"]["reverse"] == 1
                    or _POST["infos"]["effect_value"]["reverse"].lower == "true"
                )
            )

        # run the correct effect
        try:
            if POST["infos"]["effect_group"] == "static-color":
                LED_manager.effect_static(POST["infos"]["effect_value"])

            elif POST["infos"]["effect_group"] == "rainbow-static":
                LED_manager.effect_static_rainbow(
                    POST["infos"]["effect_value"]["duration"], __validate_reverse(POST)
                )

            elif POST["infos"]["effect_group"] == "rainbow-wave":
                LED_manager.effect_rainbow_wave(
                    POST["infos"]["effect_value"]["duration"], __validate_reverse(POST)
                )

            elif POST["infos"]["effect_group"] == "breathing":
                LED_manager.effect_breathe(
                    POST["infos"]["effect_value"]["colours"],
                    POST["infos"]["effect_value"]["duration"],
                )

            elif POST["infos"]["effect_group"] == "campfire":
                LED_manager.effect_candlelight(
                    POST["infos"]["effect_value"]["hue"],
                    POST["infos"]["effect_value"]["turbulence"],
                )

            elif POST["infos"]["effect_group"] == "comet":
                LED_manager.effect_comet(
                    POST["infos"]["effect_value"]["hue"],
                    POST["infos"]["effect_value"]["speed"],
                    __validate_reverse(POST),
                )

            elif POST["infos"]["effect_group"] == "rainbow-sine-static":
                LED_manager.effect_sine_static_rainbow(
                    POST["infos"]["effect_value"]["duration"],
                    __validate_reverse(POST),
                    POST["infos"]["effect_value"]["brightness"],
                )

            elif POST["infos"]["effect_group"] == "rainbow-sine-wave":
                LED_manager.effect_sine_rainbow_wave(
                    POST["infos"]["effect_value"]["duration"],
                    __validate_reverse(POST),
                    POST["infos"]["effect_value"]["brightness"],
                )

            elif POST["infos"]["effect_group"] == "progress-bar":
                LED_manager.effect_progress_bar(
                    POST["infos"]["effect_value"]["colours"],
                    POST["infos"]["effect_value"]["duration"],
                    POST["infos"]["effect_value"]["count"],
                    __validate_reverse(POST),
                )

            else:
                logging.warn("reached end of effect selection")
        except:
            logging.exception("REEE")
        finally:
            LED_manager.lock.release()
            return

    @staticmethod
    def _exit_effect_loop(timestamp_1=time.time()) -> bool:
        if not LED_manager.lock.locked():
            logging.error("lock was not locked when it should be, exiting")
            return True

        elif LED_manager.request_lock.locked():
            logging.debug("received lock request, exiting")
            return True

        timestamp_2 = time.time()
        time_to_sleep = (1 / Config.LED_FPS) - (timestamp_2 - timestamp_1)

        if time_to_sleep > 0:
            time.sleep(time_to_sleep)

        return False

    @staticmethod
    def effect_static(colour_str: str) -> None:
        """
        sets all LEDs to a static colour

        :param colour_str: color to be displayed as string
        :return:
        """
        rgb_color = HEX_to_RGB(colour_str)

        while True:
            ts1 = time.time()

            for i in range(LED_manager.strip.numPixels()):
                LED_manager.strip.setPixelColor(i, Color(*rgb_color))
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_static_rainbow(duration: float, reverse: bool) -> None:
        """
        sets all LEDs simultaneously to the colors of the rainbow in a given time period
        it uses the hsv color wheel to derive the displayed color
        it calculates the offset going back to unix 0

        :param duration: loop time in seconds
        :return:
        """
        if reverse:
            duration *= -1

        angle_per_frame = 360 / (duration * Config.LED_FPS)

        while True:
            ts1 = time.time()
            current_angle = (ts1 % duration) / duration * 360

            rgb_color = HSV_to_RGB(current_angle, 1, 1)
            for i in range(LED_manager.strip.numPixels()):
                LED_manager.strip.setPixelColor(i, Color(*rgb_color))
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_rainbow_wave(duration: float, reverse: bool) -> None:
        """
        sets all LEDs to rainbow colors in a wave pattern moving forward, looping in a given time period
        it uses the hsv color wheel to derive the displayed color
        it calculates the offset going back to unix 0

        :param duration: loop time in seconds
        :return:
        """
        if reverse:
            duration *= -1

        while True:
            ts1 = time.time()
            current_offset = (ts1 % duration) / duration * 360

            ts1 = time.time()
            for j in range(LED_manager.strip.numPixels()):
                position_on_the_wheel = (
                    (j - (LED_manager.strip.numPixels() * (current_offset / 360)))
                    / LED_manager.strip.numPixels()
                    * 360
                )
                LED_manager.strip.setPixelColor(
                    j, Color(*HSV_to_RGB(position_on_the_wheel, 1, 1))
                )
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_breathe(colours: list, duration: float) -> None:
        """
        sets all LEDs simultaneously to the given colours, transitioning between them in the given time period
        it calculates the offset going back to unix 0

        :param colours: list containing all wanted colours in order
        :param duration: transition duration between colours
        :return:
        """
        fraction_per_frame = 2 / (Config.LED_FPS * duration)

        while True:
            ts1 = time.time()

            i = int(ts1 % (duration * len(colours)) / duration)
            current_fraction = (ts1 % (duration * len(colours))) % duration / duration

            colour_a = colours[i]
            try:
                colour_b = colours[i + 1]
            except IndexError:
                colour_b = colours[0]

            colour_point = rgb_transition(
                current_fraction, *HEX_to_RGB(colour_a), *HEX_to_RGB(colour_b)
            )
            for pixel in range(LED_manager.strip.numPixels()):
                LED_manager.strip.setPixelColor(pixel, Color(*colour_point))
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

            current_fraction += fraction_per_frame

    @staticmethod
    def effect_candlelight(hue: float, turbulence: float) -> None:
        """
        Slightly flicker all LEDs to imitate a candlelight

        Args:
            hue (float): desired hue of the candlelight
            turbulence (float): amount of flicker in the effect. 1 is base turbulence 0 is none
        """
        min_brightness = 130 / turbulence if turbulence > 1 else 130
        max_variance = int(100 * turbulence)
        min_variance = 0

        while True:
            ts1 = time.time()

            current_brightness = (
                min_brightness + random.randrange(min_variance, max_variance)
            ) / 255

            rgb_color = HSV_to_RGB(hue, 1, current_brightness)

            for i in range(LED_manager.strip.numPixels()):
                LED_manager.strip.setPixelColor(i, Color(*rgb_color))
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_comet(hue: float, speed: float, reverse: bool) -> None:
        """A bright comet moving from one side of the LEDs to the other, leaving a trail behind.

        Args:
            hue (float): desired hue of the comet and it's trail
            speed (float): speed of the effect. 1 is the base speed
            reverse (bool): wether or not the comet goes in the other direction
        """
        if speed < 0:
            reverse = not reverse
            speed *= -1

        comet_width = max([int(speed), 1]) + 1  # width of the comet in pixels
        max_dimming = 0.5  # on a scale of 0 to 1
        min_dimming = 0  # on a scale of 0 to 1
        current_comet_start = comet_width

        # store every pixel as brightness (hsv) to make dimming easy
        pixels = [0 for i in range(LED_manager.strip.numPixels())]

        while True:
            ts1 = time.time()

            # dim every stored pixel randomly
            pixels = [
                pixel
                * (1 - (min_dimming + ((max_dimming - min_dimming) * random.random())))
                for pixel in pixels
            ]

            # write comet to stored pixels
            for i in range(comet_width):
                pixels[int(current_comet_start) - i - 1] = 1

            # draw every stored pixel
            for i, pixel in enumerate(pixels):
                LED_manager.strip.setPixelColor(i, Color(*HSV_to_RGB(hue, 1, pixel)))

            LED_manager.strip.show()

            if reverse:
                current_comet_start -= speed
                if current_comet_start < 0:
                    current_comet_start += LED_manager.strip.numPixels()
            else:
                current_comet_start += speed
                if current_comet_start > LED_manager.strip.numPixels():
                    current_comet_start -= LED_manager.strip.numPixels()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_sine_static_rainbow(
        duration: float, reverse: bool, brightness: float
    ) -> None:
        """
        sets all LEDs simultaneously to the colors of the rainbow in a given time period
        it uses sine waves to derive the displayed color
        it calculates the offset going back to unix 0

        > when I wrote this, only god and i understood what i was doing
        > now, god only knows

        formulas in a desmos compatible format (beware of double backslashes):
        red: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        green: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i}+\pi\cdot\\frac{4}{3})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        blue: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i}+\pi\\frac{2}{3})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        duration: T=5
        index: j=0
        total LEDs: i=150
        brightness: V=0.5

        :param duration: loop time in seconds
        :param reverse: should the effect run in reverse?
        :param brightness: brightness of the effect from 0 to 1 (0.5 is normal)
        """
        # never changing constants
        one_by_duration = 1 / duration
        obd_times_tau = one_by_duration * math.tau
        pi_by_three = math.pi / 3
        two_times_pbt = 2 * pi_by_three
        four_times_pbt = 4 * pi_by_three
        num_pixels = LED_manager.strip.numPixels()
        brightness_factor = abs(2 * brightness - 1) * -1 + 1
        brightness_summand = 255 * brightness

        if reverse:
            duration *= -1

        while True:
            # constants for every update
            ts1 = time.time()
            most_calculations = ts1 * obd_times_tau

            rgb_color = Color(
                int(
                    math.sin(most_calculations) * 127.5 * brightness_factor
                    + brightness_summand
                ),
                int(
                    math.sin(most_calculations + four_times_pbt)
                    * 127.5
                    * brightness_factor
                    + brightness_summand
                ),
                int(
                    math.sin(most_calculations + two_times_pbt)
                    * 127.5
                    * brightness_factor
                    + brightness_summand
                ),
            )

            for i in range(num_pixels):
                LED_manager.strip.setPixelColor(i, rgb_color)
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_sine_rainbow_wave(
        duration: float, reverse: bool, brightness: float
    ) -> None:
        """
        sets all LEDs to rainbow colors in a wave pattern moving forward, looping in a given time period
        it uses sine waves to derive the displayed color
        it calculates the offset going back to unix 0

        > when I wrote this, only god and i understood what i was doing
        > now, god only knows

        formulas in a desmos compatible format (beware of double backslashes):
        red: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        green: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i}+\pi\cdot\\frac{4}{3})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        blue: y=\sin(\\tau x\\frac{1}{T}+\\tau\\frac{j}{i}+\pi\\frac{2}{3})\cdot127.5\cdot(-\operatorname{abs}(2V-1)+1)+255V
        duration: T=5
        index: j=0
        total LEDs: i=150
        brightness: V=0.5

        :param duration: loop time in seconds
        :param reverse: should the effect run in reverse?
        :param brightness: brightness of the effect from 0 to 1 (0.5 is normal)
        :return:
        """
        # never changing constants
        one_by_duration = 1 / duration
        obd_times_tau = one_by_duration * math.tau
        pi_by_three = math.pi / 3
        two_times_pbt = 2 * pi_by_three
        four_times_pbt = 4 * pi_by_three
        brightness_factor = abs(2 * brightness - 1) * -1 + 1
        brightness_summand = 255 * brightness

        if reverse:
            duration *= -1

        num_pixels = LED_manager.strip.numPixels()

        while True:
            ts1 = time.time()

            for j in range(num_pixels):
                # constants for individual pixels
                j_by_pixels = j / num_pixels
                jbp_times_tau = j_by_pixels * math.tau
                most_calculations = ts1 * obd_times_tau + jbp_times_tau

                LED_manager.strip.setPixelColor(
                    j,
                    Color(
                        int(
                            math.sin(most_calculations) * 127.5 * brightness_factor
                            + brightness_summand
                        ),
                        int(
                            math.sin(most_calculations + four_times_pbt)
                            * 127.5
                            * brightness_factor
                            + brightness_summand
                        ),
                        int(
                            math.sin(most_calculations + two_times_pbt)
                            * 127.5
                            * brightness_factor
                            + brightness_summand
                        ),
                    ),
                )
            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return

    @staticmethod
    def effect_progress_bar(
        colours: list, duration: float, count: int, reverse: bool
    ) -> None:
        """Fills the LED strip like a progress bar from one side to another in a colour.
        Supports multiple progress bars chasing eachother.
        It calculates the offset going back to unix 0.

        Args:
            colours (list): list of colour codes as hex string
            duration (float): how long does it take one progress bar to complete a cycle
            count (int): how many progress bars chase each other at the same time
            reverse (bool): whether the progress bars should load in reverse
        """
        # a invalid value of 0 gets replaced with a 1
        # negative values also become positive
        section_count = abs(count) or 1

        num_leds = LED_manager.strip.numPixels()

        section_length = math.ceil(num_leds / section_count)

        while True:
            ts1 = time.time()

            index = int(ts1 % (duration * len(colours)) / duration)
            current_fraction = (ts1 % (duration * len(colours))) % duration / duration

            # invert current_fraction if displaying in reverse
            current_fraction = 1 - current_fraction if reverse else current_fraction

            for led_number in range(0, num_leds + 1):
                point_at_which_colors_switch = current_fraction * section_length

                if point_at_which_colors_switch <= (led_number % section_length):
                    led_colour = colours[index]
                else:
                    try:
                        led_colour = (
                            colours[index - 1] if reverse else colours[index + 1]
                        )
                    except IndexError:
                        led_colour = colours[0]

                LED_manager.strip.setPixelColor(
                    led_number, Color(*HEX_to_RGB(led_colour))
                )

            LED_manager.strip.show()

            if LED_manager._exit_effect_loop(ts1):
                return
